package service

import (
	"log"
	"strings"
)

//MyErr describes custom error entity
type MyErr struct {
	Status bool
	Error string `json:",omitempty"`
}


//ErrorHandler returns error for server response
func ErrorHandler(err error) MyErr {
	var hErr MyErr
	hErr.Status = true
	if err != nil{
		log.Println(err)
		hErr.Status = false
		switch true {
		case strings.Contains(err.Error(), "Cannot read body "):
			hErr.Error = "Cannot read body"
		case strings.Contains(err.Error(), "duplicate key error"):
			hErr.Error = "Duplicate entry"
		case strings.Contains(err.Error(), "Empty entry"):
			hErr.Error = "Compulsory field is empty"
		case strings.Contains(err.Error(), "Invalid data"):
			hErr.Error = "Invalid data"
		case strings.Contains(err.Error(), "Nothing to update"):
			hErr.Error = "Nothing to update"
		case strings.Contains(err.Error(), "Collection not found"):
			hErr.Error = "Collection not found"
		case strings.Contains(err.Error(), "Have no user for the task"):
			hErr.Error = "Have no user for the task"
		case strings.Contains(err.Error(), "User not found"):
			hErr.Error = "User not found"
		default:
			hErr.Error = "Something wrong"
		}
	}
	return hErr
}



