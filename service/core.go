package service

import "net/http"

//Check router
func Check(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("content-type", "application/json")
	w.Write([]byte("{\"Alive\": true}"))
}