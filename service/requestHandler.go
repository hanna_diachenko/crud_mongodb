package service

import (
	"errors"
	"encoding/json"
	"net/http"
	"crud_mongodb/database"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
)

//PrintAll responses with all elements from database
func PrintAll(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collection, err := database.GetAllCollection(vars["collection"])

	w.Header().Set("content-type", "application/json")

	if err != nil {
		hErr := ErrorHandler(err)
		json.NewEncoder(w).Encode(hErr)
		return
	}
	json.NewEncoder(w).Encode(collection)
}

//Print element by code
func Print(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collection := vars["collection"]
	code := vars["code"]

	w.Header().Set("content-type", "application/json")
	country, err := database.GetElement(collection, code)

	if err != nil {
		hErr := ErrorHandler(err)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	json.NewEncoder(w).Encode(country)
}

//Insert JSON object in database
func Insert(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	vars := mux.Vars(r)
	collection := vars["collection"]

	body, err := ioutil.ReadAll(r.Body)

	w.Header().Set("content-type", "application/json")

	if err != nil {
		log.Println(err)
		inErr := errors.New("Cannot read body ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	var dErr error

	switch collection{
	case "country":
		dErr = database.AddCountry(collection, body)
	case "user":
		dErr = database.AddUser(collection, body)
	case "tasks":
		dErr = database.AddTask(collection, body)
	case "contacts":
		dErr = database.AddContacts(collection, body)
	default:
		dErr = errors.New("Collection not found ")
	}

	hErr := ErrorHandler(dErr)
	json.NewEncoder(w).Encode(hErr)
}

//Delete removes element
func Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collection := vars["collection"]
	id := vars["id"]

	err := database.DelElement(collection, id)

	w.Header().Set("content-type", "application/json")
	hErr := ErrorHandler(err)
	json.NewEncoder(w).Encode(hErr)
}

//Update make changes in element from database
func Update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collection := vars["collection"]
	id := vars["id"]

	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)

	w.Header().Set("content-type", "application/json")

	if err != nil {
		log.Println(err)
		inErr := errors.New("Cannot read body ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	if len(body) == 0 {
		inErr := errors.New("Nothing to update ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	var dErr error

	switch collection{
	case "country":
		dErr = database.UpdateCountry(collection, id, body)
	case "user":
		dErr = database.UpdateUser(collection, id, body)
	case "tasks":
		dErr = database.UpdateTask(collection, id, body)
	case "contacts":
		dErr = database.UpdateContacts(collection, id, body)
	default:
		dErr = errors.New("Collection not found ")
	}
	hErr := ErrorHandler(dErr)
	json.NewEncoder(w).Encode(hErr)
}

//PrintTasks is responses with all user tasks from database
func PrintTasks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	result, dErr := database.UsersTasks(vars["id"])

	w.Header().Set("content-type", "application/json")

	if dErr != nil {
		hErr := ErrorHandler(dErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	json.NewEncoder(w).Encode(result)
}