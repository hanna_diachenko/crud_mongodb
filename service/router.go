package service

import (
	"net/http"

	"github.com/gorilla/mux"
)

//Router creation
func Router() http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/", Check).Methods("GET")
	router.HandleFunc("/{collection}", PrintAll).Methods("GET")
	router.HandleFunc("/{collection}/{code}", Print).Methods("GET")
	router.HandleFunc("/insert/{collection}", Insert).Methods("POST")
	router.HandleFunc("/update/{collection}/{id}", Update).Methods("PUT")
	router.HandleFunc("/remove/{collection}/{id}", Delete)

	router.HandleFunc("/user/tasks/{id}", PrintTasks).Methods("GET")

	return router
}
