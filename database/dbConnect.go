package database

import (
	"gopkg.in/mgo.v2"

	"crud_mongodb/config"
)

var db *mgo.Database

//NewSession started mongodb session
func NewSession() error {
	session, err := mgo.Dial(config.ReadDBConfig())
	if err != nil {
		return err
	}

	db = session.DB("myBase")

	return nil
}
//CloseSession of database
func CloseSession() {
	db.Session.Close()
}
