package database

import (
	"gopkg.in/mgo.v2/bson"
)

//Country describes Country entity
type Country struct {
	ID   bson.ObjectId `json:"id" bson:"_id"`
	Code       string `json:"code" bson:"code"`
	Name       string `json:"name" bson:"name"`
	Area       int    `json:"area" bson:"area"`
	Population int    `json:"population" bson:"population"`
	Capital    string `json:"capital" bson:"capital"`
}

//User describes User entity
type User struct {
	ID   bson.ObjectId `json:"id" bson:"_id"`
	Name string        `json:"name" bson:"name"`
	Age  int           `json:"age" bson:"age"`
	Male string        `json:"male" bson:"male"`
}

//Tasks describes User Tasks
type Tasks struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	UserID bson.ObjectId `json:"user_id" bson:"user_id"`
	Task   string        `json:"task" bson:"task"`
}

//Contacts describes Contacts entity
type Contacts struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	UserID bson.ObjectId `json:"user_id" bson:"user_id"`
	Mail   string        `json:"mail" bson:"mail"`
	Phone  string        `json:"phone" bson:"phone"`
}
