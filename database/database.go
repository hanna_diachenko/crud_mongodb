package database

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"errors"
	"encoding/json"
	"log"
)

func collection(name string) *mgo.Collection {
	return db.C(name)
}

func checkUnique(name string, id string) error {
	mdbCode := mgo.Index{
		Key:    []string{id},
		Unique: true,
	}

	err := db.C(name).EnsureIndex(mdbCode)
	return err
}

//Check that user is exist
func checkUser(id bson.ObjectId) error {
	log.Println(bson.ObjectId.Hex(id))
	count, err := collection("user").Find(bson.M{"_id" : id}).Count()
	if err != nil {
		return err
	}
	log.Println(count)
	if count < 1 {
		err = errors.New("User not found ")
		return err
	}
	return nil
}

// user linked with contacts one to one
func getLinkedContacts(name string, linkID string) Contacts {
	var res Contacts
	collection(name).Find(bson.M{"user_id": bson.ObjectIdHex(linkID)}).One(&res)
	return res
}

// user linked with tasks one to many
func getLinkedTasks(name string, linkID string) []Tasks {
	var res []Tasks
	collection(name).Find(bson.M{"user_id": bson.ObjectIdHex(linkID)}).All(&res)
	return res
}

func delByLinks(name string, id string) {
	switch name {
	case "user":
		uTasks := getLinkedTasks("tasks", id)
		uContacts := getLinkedContacts("contacts", id)

		log.Println("Tasks: ", uTasks, "Contacts", uContacts)

		delObj("contacts", uContacts.ID)
		for _, task := range uTasks {
			delObj("tasks", task.ID)
		}
	}
}

func delObj(name string, id bson.ObjectId) error {
	log.Println(id)
	return collection(name).Remove(bson.M{"_id": id})
}

//GetAllCollection returns all collection by name from database
func GetAllCollection(name string) ([]interface{}, error) {
	var result []interface{}

	if err := collection(name).Find(nil).All(&result); err != nil {
		return nil, err
	}

	return result, nil
}

//GetElement return element by id
func GetElement(name string, id string) (interface{}, error) {
	var result interface{}

	if err := collection(name).FindId(bson.ObjectIdHex(id)).One(&result); err != nil {
		return nil, err
	}

	return result, nil
}

//AddCountry insert new country to the collection
func AddCountry(name string, body []byte) error {
	newCountry := new(Country)

	if errData := json.Unmarshal(body, &newCountry); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	newCountry.ID = bson.NewObjectId()

	if newCountry.Code == "" {
		err := errors.New("Empty entry ")
		return err
	}

	uErr := checkUnique(name, "code")
	if uErr != nil {
		return uErr
	}

	err := collection(name).Insert(newCountry)
	return err
}

//AddUser insert new user to the collection
func AddUser(name string, body []byte) error {
	newUser := new(User)

	if errData := json.Unmarshal(body, &newUser); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	newUser.ID = bson.NewObjectId()

	err := collection(name).Insert(newUser)
	return err
}

//AddTask insert new task for user to the collection
func AddTask(name string, body []byte) error {
	newTask := new(Tasks)

	if errData := json.Unmarshal(body, &newTask); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	if newTask.UserID == "" {
		err := errors.New("Empty entry ")
		return err
	}

	uID := newTask.UserID
	log.Println(uID)
	if uErr := checkUser(uID); uErr != nil {
		return uErr
	}

	newTask.ID = bson.NewObjectId()

	err := collection(name).Insert(newTask)

	return err
}

//AddContacts insert new users contacts to the collection
func AddContacts(name string, body []byte) error {
	newContacts := new(Contacts)

	if errData := json.Unmarshal(body, &newContacts); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	if newContacts.UserID == "" {
		err := errors.New("Empty entry ")
		return err
	}

	uID := newContacts.UserID
	if uErr := checkUser(uID); uErr != nil {
		return uErr
	}

	newContacts.ID = bson.NewObjectId()

	uErr := checkUnique(name, "user_id")
	if uErr != nil {
		return uErr
	}

	err := collection(name).Insert(newContacts)
	return err
}

//DelElement by id from collection
func DelElement(name string, id string) error {
	delByLinks(name, id)
	key := bson.ObjectIdHex(id)
	err := delObj(name, key)
	return err
}

//UpdateCountry make changes in country by code
func UpdateCountry(name string, id string, body []byte) error {
	updateParam := new(Country)

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	var query = bson.M{}

	if updateParam.Code != "" {
		query["code"] = updateParam.Code
	}
	if updateParam.Name != "" {
		query["name"] = updateParam.Name
	}
	if updateParam.Area != 0 {
		query["area"] = updateParam.Area
	}
	if updateParam.Population != 0 {
		query["population"] = updateParam.Population
	}
	if updateParam.Capital != "" {
		query["capital"] = updateParam.Capital
	}

	if len(query) == 0 {
		err := errors.New("Nothing to update ")
		return err
	}

	log.Println(query)
	change := bson.M{"$set": query}

	err := collection(name).Update(bson.M{"_id": bson.ObjectIdHex(id)}, change)

	return err
}

//UpdateUser make changes in user by id
func UpdateUser(name string, id string, body []byte) error {
	updateParam := new(User)

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	var query = bson.M{}

	if updateParam.Name != "" {
		query["name"] = updateParam.Name
	}
	if updateParam.Age != 0 {
		query["age"] = updateParam.Age
	}
	if updateParam.Male != "" {
		query["male"] = updateParam.Male
	}

	if len(query) == 0 {
		err := errors.New("Nothing to update ")
		return err
	}

	log.Println(query)
	change := bson.M{"$set": query}

	err := collection(name).Update(bson.M{"_id": bson.ObjectIdHex(id)}, change)

	return err
}

//UpdateTask make changes in task by id
func UpdateTask(name string, id string, body []byte) error {
	updateParam := new(Tasks)

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	var query = bson.M{}

	if updateParam.UserID != "" {
		uID := updateParam.UserID
		if uErr := checkUser(uID); uErr != nil {
			return uErr
		}
		query["user_id"] = updateParam.UserID
	}
	if updateParam.Task != "" {
		query["task"] = updateParam.Task
	}

	if len(query) == 0 {
		err := errors.New("Nothing to update ")
		return err
	}

	log.Println(query)
	change := bson.M{"$set": query}

	err := collection(name).Update(bson.M{"_id": bson.ObjectIdHex(id)}, change)

	return err
}

//UpdateContacts make changes in task by id
func UpdateContacts(name string, id string, body []byte) error {
	updateParam := new(Contacts)

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data ")
		return err
	}

	var query = bson.M{}

	if updateParam.UserID != "" {
		uID := updateParam.UserID
		if uErr := checkUser(uID); uErr != nil {
			return uErr
		}
		query["user_id"] = updateParam.UserID
	}
	if updateParam.Mail != "" {
		query["mail"] = updateParam.Mail
	}
	if updateParam.Phone != "" {
		query["phone"] = updateParam.Phone
	}

	if len(query) == 0 {
		err := errors.New("Nothing to update ")
		return err
	}

	log.Println(query)
	change := bson.M{"$set": query}

	err := collection(name).Update(bson.M{"_id": bson.ObjectIdHex(id)}, change)

	return err
}

//UsersTasks returns users tasks by user_id
func UsersTasks(id string) ([]Tasks, error) {
	var result []Tasks

	if err :=  collection("tasks").Find(bson.M{"user_id": bson.ObjectIdHex(id)}).All(&result); err !=nil{
		return nil, err
	}
	log.Println(result)
	return result, nil
}