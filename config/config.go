package config

import (
	"os"
)

const envLogFile = "LOG_FILE"
const envDbConnection = "DB_CONNECTION"
var defaultValues = map[string]string{
	envLogFile: "/home/hdiachenko/log/crud_mongodb.log",
	envDbConnection: "localhost:27017",
}

//ReadLogFileConfig returns path to log file
func ReadLogFileConfig() string {
	return getString(envLogFile)
}

//ReadDBConfig returns database connection string
func ReadDBConfig() string{
	return getString(envDbConnection)
}

func getString(name string) string {
	var v string
	if v = os.Getenv(name); v != "" {
		return v
	}
	return defaultValues[name]
}
