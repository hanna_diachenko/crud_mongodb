package main

import (
	"log"
	"net/http"
	"os"

	"crud_mongodb/config"
	"crud_mongodb/database"
	"crud_mongodb/service"
)

var f *os.File

func init() {
	logFile := config.ReadLogFileConfig()
	file, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	f = file
	//set output of logs to f
	log.SetOutput(f)

	dbErr := database.NewSession()
	if dbErr != nil{
		log.Fatalf("Error connection to dastabase, %v", dbErr)
	}
}

func main()  {
	defer database.CloseSession()
	defer  end()

	router := service.Router()
	log.Println("API in port 8080")
	log.Println(http.ListenAndServe(":8080", router))
}

func end() {
	if f != nil {
		defer f.Close()
	}
}